﻿using GestionBiblioteca.ControlesDeUsuario;
using GestionBiblioteca.CapaNegocio;

namespace GestionBiblioteca
{
    public partial class SociosConsulta : FormularioConsulta
    {
        protected override IPersistible CrearObjetoNegocio()
        {
            return BibliotecaFactory.ObtenerInstancia().CrearSocio();
        }
        protected override UCFormularioConsulta ObtenerFormularioConsulta()
        {
            return ucFormularioConsulta;
        }
    }
}