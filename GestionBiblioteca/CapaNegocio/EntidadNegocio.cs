﻿using GestionBiblioteca.CapaDatos;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace GestionBiblioteca.CapaNegocio
{
    public abstract class EntidadNegocio<EntidadDatos> : IValidable, IPersistible
    {
        public int Id { get; set; }

        protected IValidador validador;
        protected BibliotecaEntities contexto;

        public EntidadNegocio()
        {
            contexto = Contexto.ObtenerInstancia();
        }
        public Dictionary<string, ResultadoValidacion> Validar()
        {
            return validador.Validar();
        }

        public void Alta()
        {
            EntidadDatos entity = CrearEntidadDatos();

            ObtenerDbSet().Add(entity);
            contexto.SaveChanges();

            LeerId(entity);
        }
        public void Modificacion()
        {
            EntidadDatos entity = (EntidadDatos)ObtenerDbSet().Find(this.Id);
            if (entity != null)
            {
                EscribirDatos(entity);
                contexto.SaveChanges();
            }
        }
        public void Baja()
        {
            EntidadDatos entity = (EntidadDatos)ObtenerDbSet().Find(this.Id);
            if (entity != null)
            {
                ObtenerDbSet().Remove(entity);
                contexto.SaveChanges();
            }
        }
        public virtual DataTable ObtenerDataTable(string filtro)
        {
            return Conversor.ToDataTable<EntidadDatos>(this.ObtenerList(filtro));
        }
        public virtual IList<EntidadDatos> ObtenerList(string filtro)
        {
            BibliotecaEntities contexto = Contexto.ObtenerInstancia();

            var consulta = GenerarConsultaLinq(filtro, contexto);

            IList<EntidadDatos> lista = consulta.ToList<EntidadDatos>();

            return lista;
        }
        public void ObtenerPorId(int id)
        {
            EntidadDatos entity = (EntidadDatos)ObtenerDbSet().Find(id);

            LeerDatos(entity);
        }
        public abstract EntidadDatos CrearEntidadDatos();
        public abstract DbSet ObtenerDbSet();
        public abstract void LeerId(EntidadDatos entity);
        public abstract void EscribirDatos(EntidadDatos entity);
        public abstract IQueryable<EntidadDatos> GenerarConsultaLinq(string filtro, BibliotecaEntities contexto);
        public abstract void LeerDatos(EntidadDatos entity);
    }
}