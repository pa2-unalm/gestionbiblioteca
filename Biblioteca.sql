USE [Biblioteca]
GO

/****** Object:  Table [dbo].[Autor]    Script Date: 11/11/2020 22:43:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Autor](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Apellidos] [nchar](50) NULL,
	[Nombres] [nchar](50) NULL,
	[Email] [nchar](50) NULL,
 CONSTRAINT [PK_Autores] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

USE [Biblioteca]
GO

/****** Object:  Table [dbo].[Categoria]    Script Date: 11/11/2020 22:43:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Categoria](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nchar](50) NULL,
 CONSTRAINT [PK_Categorias] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

USE [Biblioteca]
GO

/****** Object:  Table [dbo].[Editorial]    Script Date: 11/11/2020 22:44:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Editorial](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nchar](50) NULL,
 CONSTRAINT [PK_Editoriales] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

USE [Biblioteca]
GO

/****** Object:  Table [dbo].[Idioma]    Script Date: 11/11/2020 22:44:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Idioma](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nchar](20) NOT NULL,
 CONSTRAINT [PK_Idioma] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

USE [Biblioteca]
GO

/****** Object:  Table [dbo].[Libro]    Script Date: 11/11/2020 22:44:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Libro](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ISBN] [nchar](20) NULL,
	[Titulo] [nchar](50) NULL,
	[FechaLanzamiento] [date] NULL,
	[IdAutor] [int] NULL,
	[IdCategoria] [int] NULL,
	[IdEditorial] [int] NULL,
	[IdIdioma] [int] NULL,
	[Paginas] [int] NULL,
	[Descripcion] [nchar](256) NULL,
	[Peso] [int] NULL,
 CONSTRAINT [PK_Libros] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
USE [Biblioteca]
GO

/****** Object:  Table [dbo].[Prestamo]    Script Date: 11/11/2020 22:44:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Prestamo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdSocio] [int] NULL,
	[IdLibro] [int] NULL,
	[FechaSalida] [date] NULL,
	[FechaEntrada] [date] NULL,
	[Observaciones] [nchar](256) NULL,
 CONSTRAINT [PK_Prestamo] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

USE [Biblioteca]
GO

/****** Object:  Table [dbo].[Socio]    Script Date: 11/11/2020 22:45:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Socio](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DNI] [int] NULL,
	[Apellidos] [nchar](50) NULL,
	[Nombres] [nchar](50) NULL,
	[Telefono] [nchar](13) NULL,
	[Direccion] [nchar](50) NULL,
	[CodigoPostal] [nchar](10) NULL,
	[Observaciones] [nchar](256) NULL,
	[Email] [nchar](50) NULL,
 CONSTRAINT [PK_Lectores] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO




ALTER TABLE [dbo].[Libro]  WITH CHECK ADD  CONSTRAINT [FK_Libro_Idioma] FOREIGN KEY([IdIdioma])
REFERENCES [dbo].[Idioma] ([Id])
GO

ALTER TABLE [dbo].[Libro] CHECK CONSTRAINT [FK_Libro_Idioma]
GO

ALTER TABLE [dbo].[Libro]  WITH CHECK ADD  CONSTRAINT [FK_Libros_Autores] FOREIGN KEY([IdAutor])
REFERENCES [dbo].[Autor] ([Id])
GO

ALTER TABLE [dbo].[Libro] CHECK CONSTRAINT [FK_Libros_Autores]
GO

ALTER TABLE [dbo].[Libro]  WITH CHECK ADD  CONSTRAINT [FK_Libros_Categorias] FOREIGN KEY([IdCategoria])
REFERENCES [dbo].[Categoria] ([Id])
GO

ALTER TABLE [dbo].[Libro] CHECK CONSTRAINT [FK_Libros_Categorias]
GO

ALTER TABLE [dbo].[Libro]  WITH CHECK ADD  CONSTRAINT [FK_Libros_Editoriales] FOREIGN KEY([IdEditorial])
REFERENCES [dbo].[Editorial] ([Id])
GO

ALTER TABLE [dbo].[Libro] CHECK CONSTRAINT [FK_Libros_Editoriales]
GO


ALTER TABLE [dbo].[Prestamo]  WITH CHECK ADD  CONSTRAINT [FK_Alquileres_Libros] FOREIGN KEY([IdLibro])
REFERENCES [dbo].[Libro] ([Id])
GO

ALTER TABLE [dbo].[Prestamo] CHECK CONSTRAINT [FK_Alquileres_Libros]
GO

ALTER TABLE [dbo].[Prestamo]  WITH CHECK ADD  CONSTRAINT [FK_Alquileres_Socios] FOREIGN KEY([IdSocio])
REFERENCES [dbo].[Socio] ([Id])
GO

ALTER TABLE [dbo].[Prestamo] CHECK CONSTRAINT [FK_Alquileres_Socios]
GO


USE [Biblioteca]
GO
