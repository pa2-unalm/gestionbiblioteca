﻿using System.Collections.Generic;
using System.Linq;

namespace GestionBiblioteca.CapaNegocio
{
    public class PrestamoValidador : Validador
    {
        private readonly PrestamoNeg prestamo;

        public PrestamoValidador(IValidable entidad)
            : base(entidad)
        {

            this.prestamo = (PrestamoNeg)entidad;
        }
        public override Dictionary<string, ResultadoValidacion> Validar()
        {
            base.Validar();

            ValidarFechas("Fechas");

            return Resultados;
        }
        private void ValidarFechas(string campo)
        {
            NuevoResultadoValidacion(campo);

            if (prestamo.FechaEntrada > prestamo.FechaSalida)
                PonerMensajeError("La fecha de entrada no puede ser mayor a la fecha de salida. ", campo, campo);

        }
    }
}