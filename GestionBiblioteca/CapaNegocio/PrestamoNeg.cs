﻿using GestionBiblioteca.CapaDatos;
using System;
using System.Collections;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.UI.WebControls;

namespace GestionBiblioteca.CapaNegocio
{
    public class PrestamoNeg : EntidadNegocio<CapaDatos.Prestamo>
    {
        public SocioNeg Socio { get; set; }
        public LibroNeg Libro { get; set; }
        public DateTime FechaSalida { get; set; }
        public DateTime FechaEntrada { get; set; }
        public string Observaciones { get; set; }
        
        public PrestamoNeg()
        {
            validador = new PrestamoValidador(this);
            this.Socio = new SocioNeg();
            this.Libro = new LibroNeg();
        }
        public override CapaDatos.Prestamo CrearEntidadDatos()
        {
            return new CapaDatos.Prestamo
            {
                Socio = contexto.Socio.Find(this.Socio.Id),
                Libro = contexto.Libro.Find(this.Libro.Id),
                FechaSalida = this.FechaSalida,
                FechaEntrada = this.FechaEntrada,
                Observaciones = this.Observaciones
            };
        }
        public override DbSet ObtenerDbSet()
        {
            return contexto.Prestamo;
        }

        public override void LeerId(CapaDatos.Prestamo entity)
        {
            this.Id = entity.Id;
        }

        public override void EscribirDatos(CapaDatos.Prestamo entity)
        {
            entity.Socio = contexto.Socio.Find(this.Socio.Id);
            entity.Libro = contexto.Libro.Find(this.Libro.Id);
            entity.FechaSalida = this.FechaSalida;
            entity.FechaEntrada = this.FechaEntrada;
            entity.Observaciones = this.Observaciones;
        }

        public override IQueryable<CapaDatos.Prestamo> GenerarConsultaLinq(string filtro, BibliotecaEntities contexto)
        {
            return from l in contexto.Prestamo
                   where l.Socio.Apellidos.Contains(filtro) || l.Socio.Nombres.Contains(filtro) ||
                        l.Libro.Titulo.Contains(filtro) || l.Libro.Titulo.Contains(filtro) || l.Libro.Descripcion.Contains(filtro) || l.Libro.Editorial.Nombre.Contains(filtro) || l.Libro.Autor.Apellidos.Contains(filtro) ||
                        l.Observaciones.Contains(filtro)
                   select l;
        }

        public override void LeerDatos(CapaDatos.Prestamo entity)
        {
            LeerId(entity);
            this.Socio.ObtenerPorId(entity.Socio.Id);
            this.Libro.ObtenerPorId(entity.Libro.Id);
            this.FechaSalida = (DateTime)entity.FechaSalida;
            this.FechaEntrada = (DateTime)entity.FechaEntrada;
            this.Observaciones = entity.Observaciones;
        }
        public override DataTable ObtenerDataTable(string filtro)
        {
            BibliotecaEntities contexto = Contexto.ObtenerInstancia();

            var consulta = GenerarConsultaLinq(filtro, contexto);

            return ListaADataTable(consulta.ToList());
        }
        private DataTable ListaADataTable(IList lista)
        {
            DataTable tabla = new DataTable();

            tabla.Columns.Add("Id");
            tabla.Columns.Add("Socio");
            tabla.Columns.Add("Libro");
            tabla.Columns.Add("Fecha Salida");
            tabla.Columns.Add("Fecha Entrada");
            tabla.Columns.Add("Observaciones");

            foreach (Prestamo prestamo in lista)
            {
                DataRow row = tabla.NewRow();
                row["Id"] = prestamo.Id;
                row["Socio"] = prestamo.Socio.Apellidos + ", " + prestamo.Socio.Nombres;
                row["Libro"] = prestamo.Libro.Titulo + "(" + prestamo.Libro.Editorial.Nombre + ")";
                row["Fecha Salida"] = prestamo.FechaSalida.ToString();
                row["Fecha Entrada"] = prestamo.FechaEntrada.ToString();
                row["Observaciones"] = prestamo.Observaciones;
                tabla.Rows.Add(row);
            }
            return tabla;
        }
    }
}