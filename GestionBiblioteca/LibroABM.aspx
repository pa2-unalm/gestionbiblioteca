﻿<%@ Page Title="Alta de Libro" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="LibroABM.aspx.cs" Inherits="GestionBiblioteca.LibroABM" %>
<%@ Register src="~/ControlesDeUsuario/UCMensajeUsuario.ascx" tagname="UCMensajeUsuario" tagprefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <div class="row d-flex">
        <div class="col-md-12 form-group justify-content-center">
            <uc:UCMensajeUsuario ID="ucMensajes" runat="server" Visible="False"></uc:UCMensajeUsuario>
        </div>
    </div>
    <asp:Panel ID="PanelFormulario" runat="server">
        <div class="row d-flex">
            <div class="col-md-4 form-group justify-content-center">
                <asp:TextBox ID="txtId" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
            
                <asp:Label ID="Label1" runat="server" Text="ISBN" CssClass="bmd-label-floating" AssociatedControlID="txtISBN"></asp:Label>
                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtISBN" ErrorMessage="Debe completar este campo." ForeColor="#CC0000"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtISBN" runat="server" CssClass="form-control"></asp:TextBox>
            
                <asp:Label ID="Label3" runat="server" Text="Título" AssociatedControlID="txtTitulo"></asp:Label>
                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtTitulo" ErrorMessage="Debe completar este campo." ForeColor="#CC0000"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtTitulo" runat="server" CssClass="form-control"></asp:TextBox>
            
                <asp:Label ID="Label2" runat="server" Text="Fecha de Lanzamiento" AssociatedControlID="txtFechaLanzamiento"></asp:Label>
                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtFechaLanzamiento" ErrorMessage="Debe completar este campo." ForeColor="#CC0000"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtFechaLanzamiento" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:Label ID="Label4" runat="server" Text="Autor" AssociatedControlID="ddlAutor"></asp:Label>
                &nbsp;<asp:DropDownList ID="ddlAutor" runat="server" CssClass="form-control"></asp:DropDownList>
                
                <asp:Label ID="Label8" runat="server" Text="Descripción" CssClass="bmd-label-floating" AssociatedControlID="txtDescripcion"></asp:Label>
                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtDescripcion" ErrorMessage="Debe completar este campo." ForeColor="#CC0000"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtDescripcion" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="3"></asp:TextBox>
            </div>
            <div class="col-md-4 form-group justify-content-center">
                <asp:Label ID="Label5" runat="server" Text="Categoría" AssociatedControlID="ddlCategoria"></asp:Label>
                &nbsp;<asp:DropDownList ID="ddlCategoria" CssClass="form-control" runat="server"></asp:DropDownList>
                
                <asp:Label ID="Label6" runat="server" Text="Editorial" AssociatedControlID="ddlEditorial"></asp:Label>
                &nbsp;<asp:DropDownList ID="ddlEditorial" CssClass="form-control" runat="server"></asp:DropDownList>
                
                <asp:Label ID="Label7" runat="server" Text="Idioma" AssociatedControlID="ddlIdioma"></asp:Label>
                &nbsp;<asp:DropDownList ID="ddlIdioma" CssClass="form-control" runat="server"></asp:DropDownList>
                
                <asp:Label ID="Label16" runat="server" Text="Páginas" AssociatedControlID="txtPaginas"></asp:Label>
                &nbsp;<asp:TextBox ID="txtPaginas" runat="server" CssClass="form-control"></asp:TextBox>

                <asp:Label ID="Label9" runat="server" Text="Peso" AssociatedControlID="txtPeso"></asp:Label>
                &nbsp;<asp:TextBox ID="txtPeso" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="row d-flex">
            <div class="col-md-4 justify-content-end">
                <asp:Button ID="btnOk" runat="server" Text="Ok" UseSubmitBehavior="true" CssClass="btn btn-primary"/>
                <input id="btnCancelar" type="button" value="Cancelar" onclick="window.history.back()" class="btn btn-secondary" />
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="PanelContinuar" runat="server" Visible="false" >
        <div class="row d-flex">
            <div class="col-md-4 justify-content-end">
                <asp:Button ID="btnContinuar" runat="server" Text="Continuar" UseSubmitBehavior="true" CssClass="btn btn-primary" PostBackUrl="~/LibrosConsulta.aspx"/>
            </div>
        </div>
    </asp:Panel>
</asp:Content>