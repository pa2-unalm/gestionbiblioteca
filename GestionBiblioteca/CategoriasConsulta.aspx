﻿<%@ Page Title="Categorías" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CategoriasConsulta.aspx.cs" Inherits="GestionBiblioteca.CategoriasConsulta" %>
<%@ Register src="~/ControlesDeUsuario/UCFormularioConsulta.ascx" tagname="UCFormularioConsulta" tagprefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <uc:UCFormularioConsulta ID="ucFormularioConsulta" runat="server" Titulo="Categorías" NombreFormularioABM="CategoriaABM.aspx"></uc:UCFormularioConsulta>
</asp:Content>