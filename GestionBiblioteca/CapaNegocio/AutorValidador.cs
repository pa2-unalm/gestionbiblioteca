﻿using Microsoft.Ajax.Utilities;
using System.Collections.Generic;
using System.Linq;

namespace GestionBiblioteca.CapaNegocio
{
    public class AutorValidador : Validador
    {
        private readonly AutorNeg autor;

        public AutorValidador(IValidable entidad)
            : base(entidad)
        {

            this.autor = (AutorNeg)entidad;
        }
        public override Dictionary<string, ResultadoValidacion> Validar()
        {
            base.Validar();

            ValidarEmail("EMail");

            return Resultados;
        }
        private void ValidarEmail(string campo)
        {
            NuevoResultadoValidacion(campo);

            if (autor.Email.IsNullOrWhiteSpace() || !autor.Email.Contains("@"))
                PonerMensajeError("El campo {0} tiene un formato inválido. ", campo, campo);

            if (Contexto.ObtenerInstancia().Socio.Any(s => s.Email.Equals(autor.Email)))
                PonerMensajeError("El email '{0}' ya existe. ", autor.Email, campo);
        }
    }
}