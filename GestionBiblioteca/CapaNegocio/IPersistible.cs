﻿using System.Collections.Generic;
using System.Data;

namespace GestionBiblioteca.CapaNegocio
{
    public interface IPersistible
    {
        public void Alta();
        public void Modificacion();
        public void Baja();
        public DataTable ObtenerDataTable(string filtro);
        public void ObtenerPorId(int id);
    }
}
