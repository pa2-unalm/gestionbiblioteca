﻿using GestionBiblioteca.CapaNegocio;

namespace GestionBiblioteca
{
    public partial class AutorABM : FormularioABM
    {
        private AutorNeg entidad;
        protected override void BloquearEdicionCampos()
        {
            txtId.ReadOnly = true;
            txtApellidos.ReadOnly = true;
            txtNombres.ReadOnly = true;
            txtEmail.ReadOnly = true;
        }
        protected override void CargarCampos()
        {
            txtId.Text = entidad.Id.ToString().Trim();
            txtApellidos.Text = entidad.Apellidos.Trim();
            txtNombres.Text = entidad.Nombres.Trim();
            txtEmail.Text = entidad.Email.Trim();
        }
        protected override void ConfigurarAlta()
        {
            Title = "Alta de Autor";
        }
        protected override void ConfigurarBaja()
        {
            Title = "Eliminar Autor";
            BloquearEdicionCampos();
        }
        protected override void ConfigurarModificacion()
        {
            Title = "Editar Autor";
        }
        protected override void CrearObjeto()
        {
            BibliotecaFactory factory = BibliotecaFactory.ObtenerInstancia();
            entidad = (AutorNeg)factory.CrearAutor();
        }
        protected override void DarAlta()
        {
            entidad.Alta();
        }
        protected override void DarBaja()
        {
            entidad.Baja();
        }
        protected override void GuardarCambios()
        {
            entidad.Modificacion();
        }
        protected override void InicializarMensajes()
        {
            ucMensajes.Inicializar();
        }
        protected override void LeerCampos()
        {
            int.TryParse(txtId.Text, out int socioId);
            entidad.Id = socioId;
            entidad.Apellidos = txtApellidos.Text;
            entidad.Nombres = txtNombres.Text;
            entidad.Email = txtEmail.Text;
        }
        protected override void LimpiarFormulario()
        {
            txtId.Text = "";
            txtApellidos.Text = "";
            txtNombres.Text = "";
            txtEmail.Text = "";
        }
        protected override void MensajeAltaError()
        {
            ucMensajes.MostrarError();
        }
        protected override void MensajeAltaOK()
        {
            ucMensajes.MostrarOk("Alta de Autor efectuada correctamente.");
        }
        protected override void MensajeBajaOK()
        {
            ucMensajes.MostrarOk("Baja de Autor efectuada correctamente.");
        }
        protected override void MensajeModificacionOK()
        {
            ucMensajes.MostrarOk("Edición de Autor efectuada correctamente.");
        }
        protected override void ObtenerEntidad()
        {
            entidad.ObtenerPorId(Id);
        }
        protected override void OcultarFormulario()
        {
            PanelFormulario.Visible = false;
            PanelContinuar.Visible = true;
        }
        protected override bool ReglasNegocioOK()
        {
            return Validador.ValidarReglasNegocio(entidad, ucMensajes);
        }
    }
}