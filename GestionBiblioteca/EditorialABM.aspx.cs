﻿using GestionBiblioteca.CapaNegocio;

namespace GestionBiblioteca
{
    public partial class EditorialABM : FormularioABM
    {
        private EditorialNeg entidad;
        protected override void BloquearEdicionCampos()
        {
            txtId.ReadOnly = true;
            txtNombre.ReadOnly = true;
        }

        protected override void CargarCampos()
        {
            txtId.Text = entidad.Id.ToString().Trim();
            txtNombre.Text = entidad.Nombre.Trim();
        }

        protected override void ConfigurarAlta()
        {
            Title = "Alta de Editorial";
        }

        protected override void ConfigurarBaja()
        {
            Title = "Eliminar Editorial";
            BloquearEdicionCampos();
        }

        protected override void ConfigurarModificacion()
        {
            Title = "Editar Editorial";
        }

        protected override void CrearObjeto()
        {
            BibliotecaFactory factory = BibliotecaFactory.ObtenerInstancia();
            entidad = factory.CrearEditorial();
        }

        protected override void DarAlta()
        {
            entidad.Alta();
        }

        protected override void DarBaja()
        {
            entidad.Baja();
        }

        protected override void GuardarCambios()
        {
            entidad.Modificacion();
        }

        protected override void InicializarMensajes()
        {
            ucMensajes.Inicializar();
        }

        protected override void LeerCampos()
        {
            int.TryParse(txtId.Text, out int entidadId);
            entidad.Id = entidadId;
            entidad.Nombre = txtNombre.Text;
        }

        protected override void LimpiarFormulario()
        {
            txtId.Text = "";
            txtNombre.Text = "";
        }

        protected override void MensajeAltaError()
        {
            ucMensajes.MostrarError();
        }

        protected override void MensajeAltaOK()
        {
            ucMensajes.MostrarOk("Alta de Editorial efectuada correctamente.");
        }

        protected override void MensajeBajaOK()
        {
            ucMensajes.MostrarOk("Baja de Editorial efectuada correctamente.");
        }

        protected override void MensajeModificacionOK()
        {
            ucMensajes.MostrarOk("Edición de Editorial efectuada correctamente.");
        }

        protected override void ObtenerEntidad()
        {
            entidad.ObtenerPorId(Id);
        }

        protected override void OcultarFormulario()
        {
            PanelFormulario.Visible = false;
            PanelContinuar.Visible = true;
        }

        protected override bool ReglasNegocioOK()
        {
            return Validador.ValidarReglasNegocio(entidad, ucMensajes);
        }
    }
}