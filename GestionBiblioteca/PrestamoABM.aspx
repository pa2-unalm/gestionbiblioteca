﻿<%@ Page Title="Alta de Préstamo" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PrestamoABM.aspx.cs" Inherits="GestionBiblioteca.PrestamoABM" %>
<%@ Register src="~/ControlesDeUsuario/UCMensajeUsuario.ascx" tagname="UCMensajeUsuario" tagprefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <div class="row d-flex">
        <div class="col-md-12 form-group justify-content-center">
            <uc:UCMensajeUsuario ID="ucMensajes" runat="server" Visible="False"></uc:UCMensajeUsuario>
        </div>
    </div>
    <asp:Panel ID="PanelFormulario" runat="server">
        <div class="row d-flex">
            <div class="col-md-4 form-group justify-content-center">
                <asp:TextBox ID="txtId" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
            
                <asp:Label ID="Label1" runat="server" Text="Socio" AssociatedControlID="ddlSocio"></asp:Label>
                &nbsp;<asp:DropDownList ID="ddlSocio" CssClass="form-control" runat="server"></asp:DropDownList>
                
                <asp:Label ID="Label3" runat="server" Text="Libro" AssociatedControlID="ddlLibro"></asp:Label>
                &nbsp;<asp:DropDownList ID="ddlLibro" CssClass="form-control" runat="server"></asp:DropDownList>
            </div>
            <div class="col-md-4 form-group justify-content-center">
                <asp:Label ID="Label2" runat="server" Text="Fecha de Salida" AssociatedControlID="txtFechaSalida"></asp:Label>
                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtFechaSalida" ErrorMessage="Debe completar este campo." ForeColor="#CC0000"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtFechaSalida" runat="server" CssClass="form-control"></asp:TextBox>

                <asp:Label ID="Label4" runat="server" Text="Fecha de Entrada" AssociatedControlID="txtFechaEntrada"></asp:Label>
                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFechaEntrada" ErrorMessage="Debe completar este campo." ForeColor="#CC0000"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtFechaEntrada" runat="server" CssClass="form-control"></asp:TextBox>

                <asp:Label ID="Label8" runat="server" Text="Observaciones" CssClass="bmd-label-floating" AssociatedControlID="txtObservaciones"></asp:Label>
                <asp:TextBox ID="txtObservaciones" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="3"></asp:TextBox>
            </div>
        </div>
        <div class="row d-flex">
            <div class="col-md-4 justify-content-end">
                <asp:Button ID="btnOk" runat="server" Text="Ok" UseSubmitBehavior="true" CssClass="btn btn-primary"/>
                <input id="btnCancelar" type="button" value="Cancelar" onclick="window.history.back()" class="btn btn-secondary" />
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="PanelContinuar" runat="server" Visible="false" >
        <div class="row d-flex">
            <div class="col-md-4 justify-content-end">
                <asp:Button ID="btnContinuar" runat="server" Text="Continuar" UseSubmitBehavior="true" CssClass="btn btn-primary" PostBackUrl="~/PrestamosConsulta.aspx"/>
            </div>
        </div>
    </asp:Panel>
</asp:Content>