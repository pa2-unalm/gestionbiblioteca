﻿using GestionBiblioteca.CapaNegocio;
using System;

namespace GestionBiblioteca
{
    public partial class LibroABM : FormularioABM
    {
        private LibroNeg entidad;
        protected override void BloquearEdicionCampos()
        {
            txtId.ReadOnly = true;
            txtISBN.ReadOnly = true;
            txtTitulo.ReadOnly = true;
            txtFechaLanzamiento.ReadOnly = true;
            ddlAutor.Enabled = false;
            ddlCategoria.Enabled = false;
            ddlEditorial.Enabled = false;
            ddlIdioma.Enabled = false;
            txtPaginas.ReadOnly = true;
            txtDescripcion.ReadOnly = true;
            txtPeso.ReadOnly = true;
        }
        protected override void CargarCampos()
        {
            txtId.Text = entidad.Id.ToString().Trim();
            txtISBN.Text = entidad.ISBN.Trim();
            txtTitulo.Text = entidad.Titulo.Trim();
            txtFechaLanzamiento.Text = entidad.FechaLanzamiento.ToString().Trim();
            ddlAutor.SelectedValue = entidad.Autor.Id.ToString();
            ddlCategoria.SelectedValue = entidad.Categoria.Id.ToString();
            ddlEditorial.SelectedValue = entidad.Categoria.Id.ToString();
            ddlIdioma.SelectedValue = entidad.Idioma.Id.ToString();
            txtPaginas.Text = entidad.Paginas.ToString();
            txtDescripcion.Text = entidad.Descripcion.Trim();
            txtPeso.Text = entidad.Peso.ToString();
        }
        protected override void ConfigurarAlta()
        {
            Title = "Alta de Libro";
        }
        protected override void ConfigurarBaja()
        {
            Title = "Eliminar Libro";
            BloquearEdicionCampos();
        }
        protected override void ConfigurarModificacion()
        {
            Title = "Editar Libro";
        }
        protected override void CrearObjeto()
        {
            BibliotecaFactory factory = BibliotecaFactory.ObtenerInstancia();
            entidad = factory.CrearLibro();
        }
        protected override void DarAlta()
        {
            entidad.Alta();
        }
        protected override void DarBaja()
        {
            entidad.Baja();
        }
        protected override void GuardarCambios()
        {
            entidad.Modificacion();
        }
        protected override void Inicializar()
        {
            base.Inicializar();
            InicializarDropDownLists();
        }
        private void InicializarDropDownLists()
        {
            Conversor.LlenarDropDownList(ddlAutor, entidad.Autor, "Apellidos");
            Conversor.LlenarDropDownList(ddlCategoria, entidad.Categoria, "Nombre");
            Conversor.LlenarDropDownList(ddlEditorial, entidad.Editorial, "Nombre");
            Conversor.LlenarDropDownList(ddlIdioma, entidad.Idioma, "Nombre");
        }
        protected override void InicializarMensajes()
        {
            ucMensajes.Inicializar();
        }
        protected override void LeerCampos()
        {
            int.TryParse(txtId.Text, out int libroId);
            entidad.Id = libroId;
            entidad.ISBN = txtISBN.Text;
            entidad.Titulo = txtTitulo.Text;
            entidad.FechaLanzamiento = DateTime.Parse(txtFechaLanzamiento.Text);
            entidad.Autor.ObtenerPorId(int.Parse(ddlAutor.SelectedValue));
            entidad.Categoria.ObtenerPorId(int.Parse(ddlCategoria.SelectedValue));
            entidad.Editorial.ObtenerPorId(int.Parse(ddlEditorial.SelectedValue));
            entidad.Idioma.ObtenerPorId(int.Parse(ddlIdioma.SelectedValue));
            entidad.Paginas = int.Parse(txtPaginas.Text);
            entidad.Descripcion = txtDescripcion.Text.Trim();
            entidad.Peso = int.Parse(txtPeso.Text);
        }
        protected override void LimpiarFormulario()
        {
            txtId.Text = "";
            txtISBN.Text = "";
            txtTitulo.Text = "";
            txtFechaLanzamiento.Text = "";
            ddlAutor.SelectedIndex = 0;
            ddlCategoria.SelectedIndex = 0;
            ddlEditorial.SelectedIndex = 0;
            ddlIdioma.SelectedIndex = 0;
            txtPaginas.Text = "";
            txtDescripcion.Text = "";
            txtPeso.Text = "";
        }
        protected override void MensajeAltaError()
        {
            ucMensajes.MostrarError();
        }
        protected override void MensajeAltaOK()
        {
            ucMensajes.MostrarOk("Alta de Libro efectuada correctamente.");
        }
        protected override void MensajeBajaOK()
        {
            ucMensajes.MostrarOk("Baja de Libro efectuada correctamente.");
        }
        protected override void MensajeModificacionOK()
        {
            ucMensajes.MostrarOk("Edición de Libro efectuada correctamente.");
        }
        protected override void ObtenerEntidad()
        {
            entidad.ObtenerPorId(Id);
        }
        protected override void OcultarFormulario()
        {
            PanelFormulario.Visible = false;
            PanelContinuar.Visible = true;
        }
        protected override bool ReglasNegocioOK()
        {
            return Validador.ValidarReglasNegocio(entidad, ucMensajes);
        }
    }
}