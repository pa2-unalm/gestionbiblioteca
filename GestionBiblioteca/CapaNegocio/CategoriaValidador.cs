﻿using System.Collections.Generic;
using System.Linq;

namespace GestionBiblioteca.CapaNegocio
{
    public class CategoriaValidador : Validador
    {
        private readonly CategoriaNeg categoria;

        public CategoriaValidador(IValidable entidad)
            : base(entidad)
        {
            this.categoria = (CategoriaNeg)entidad;
        }
        public override Dictionary<string, ResultadoValidacion> Validar()
        {
            base.Validar();

            ValidarNombre("Nombre");

            return Resultados;
        }
        private void ValidarNombre(string campo)
        {
            NuevoResultadoValidacion(campo);

            if (Contexto.ObtenerInstancia().Categoria.Any(c => c.Nombre.Equals(categoria.Nombre)))
                PonerMensajeError("La Categoría '{0}' ya existe. ", categoria.Nombre.ToString(), campo);
        }
    }
}