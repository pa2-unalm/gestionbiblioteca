﻿<%@ Page Title="Editoriales" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditorialesConsulta.aspx.cs" Inherits="GestionBiblioteca.EditorialesConsulta" %>
<%@ Register src="~/ControlesDeUsuario/UCFormularioConsulta.ascx" tagname="UCFormularioConsulta" tagprefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <uc:UCFormularioConsulta ID="ucFormularioConsulta" runat="server" Titulo="Editoriales" NombreFormularioABM="EditorialABM.aspx"></uc:UCFormularioConsulta>
</asp:Content>