﻿<%@ Page Title="Autores" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AutoresConsulta.aspx.cs" Inherits="GestionBiblioteca.AutoresConsulta" %>
<%@ Register src="~/ControlesDeUsuario/UCFormularioConsulta.ascx" tagname="UCFormularioConsulta" tagprefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <uc:UCFormularioConsulta ID="ucFormularioConsulta" runat="server" Titulo="Autores" NombreFormularioABM="AutorABM.aspx"></uc:UCFormularioConsulta>
</asp:Content>