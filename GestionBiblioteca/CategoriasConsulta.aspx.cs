﻿using GestionBiblioteca.CapaNegocio;
using GestionBiblioteca.ControlesDeUsuario;

namespace GestionBiblioteca
{
    public partial class CategoriasConsulta : FormularioConsulta
    {
        protected override IPersistible CrearObjetoNegocio()
        {
            return BibliotecaFactory.ObtenerInstancia().CrearCategoria();
        }
        protected override UCFormularioConsulta ObtenerFormularioConsulta()
        {
            return ucFormularioConsulta;
        }
    }
}