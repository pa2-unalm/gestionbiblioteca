﻿using GestionBiblioteca.CapaNegocio;
using GestionBiblioteca.ControlesDeUsuario;

namespace GestionBiblioteca
{
    public partial class EditorialesConsulta : FormularioConsulta
    {
        protected override IPersistible CrearObjetoNegocio()
        {
            return BibliotecaFactory.ObtenerInstancia().CrearEditorial();
        }
        protected override UCFormularioConsulta ObtenerFormularioConsulta()
        {
            return ucFormularioConsulta;
        }
    }
}