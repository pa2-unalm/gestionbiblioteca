﻿using GestionBiblioteca.CapaNegocio;
using GestionBiblioteca.ControlesDeUsuario;

namespace GestionBiblioteca
{
    public partial class AutoresConsulta : FormularioConsulta
    {
        protected override IPersistible CrearObjetoNegocio()
        {
            return BibliotecaFactory.ObtenerInstancia().CrearAutor();
        }
        protected override UCFormularioConsulta ObtenerFormularioConsulta()
        {
            return ucFormularioConsulta;
        }
    }
}