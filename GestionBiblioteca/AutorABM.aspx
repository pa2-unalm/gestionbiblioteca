﻿<%@ Page Title="Alta de Autor" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AutorABM.aspx.cs" Inherits="GestionBiblioteca.AutorABM" %>
<%@ Register src="~/ControlesDeUsuario/UCMensajeUsuario.ascx" tagname="UCMensajeUsuario" tagprefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <div class="row d-flex">
        <div class="col-md-12 form-group justify-content-center">
            <uc:UCMensajeUsuario ID="ucMensajes" runat="server" Visible="False"></uc:UCMensajeUsuario>
        </div>
    </div>
    <asp:Panel ID="PanelFormulario" runat="server">
        <div class="row d-flex">
            <div class="col-md-4 form-group justify-content-center">
                <asp:TextBox ID="txtId" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
            
                <asp:Label ID="Label3" runat="server" Text="Apellido/s" AssociatedControlID="txtApellidos"></asp:Label>
                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtApellidos" ErrorMessage="Debe completar este campo." ForeColor="#CC0000"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtApellidos" runat="server" CssClass="form-control"></asp:TextBox>
            
                <asp:Label ID="Label2" runat="server" Text="Nombre/s" AssociatedControlID="txtNombres"></asp:Label>
                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtNombres" ErrorMessage="Debe completar este campo." ForeColor="#CC0000"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtNombres" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-md-4 form-group justify-content-center">    
                <asp:Label ID="Label15" runat="server" Text="e-mail" AssociatedControlID="txtEmail"></asp:Label>
                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtEmail" ErrorMessage="Debe completar este campo." ForeColor="#CC0000"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="row d-flex">
            <div class="col-md-4 justify-content-end">
                <asp:Button ID="btnOk" runat="server" Text="Ok" UseSubmitBehavior="true" CssClass="btn btn-primary"/>
                <input id="btnCancelar" type="button" value="Cancelar" onclick="window.history.back()" class="btn btn-secondary" />
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="PanelContinuar" runat="server" Visible="false" >
        <div class="row d-flex">
            <div class="col-md-4 justify-content-end">
                <asp:Button ID="btnContinuar" runat="server" Text="Continuar" UseSubmitBehavior="true" CssClass="btn btn-primary" PostBackUrl="~/AutoresConsulta.aspx"/>
            </div>
        </div>
    </asp:Panel>
</asp:Content>