﻿using GestionBiblioteca.CapaDatos;
using System;
using System.Collections;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.UI.WebControls;

namespace GestionBiblioteca.CapaNegocio
{
    public class LibroNeg : EntidadNegocio<CapaDatos.Libro>
    {
        public string ISBN { get; set; }
        public string Titulo { get; set; }
        public DateTime FechaLanzamiento { get; set; }
        public AutorNeg Autor { get; set; }
        public CategoriaNeg Categoria { get; set; }
        public EditorialNeg Editorial { get; set; }
        public IdiomaNeg Idioma { get; set; }
        public int? Paginas { get; set; }
        public string Descripcion { get; set; }
        public int? Peso { get; set; }

        public LibroNeg()
        {
            validador = new LibroValidador(this);
            this.Autor = new AutorNeg();
            this.Editorial = new EditorialNeg();
            this.Categoria = new CategoriaNeg();
            this.Idioma = new IdiomaNeg();
        }
        public override CapaDatos.Libro CrearEntidadDatos()
        {
            return new CapaDatos.Libro
            {
                ISBN = this.ISBN,
                Titulo = this.Titulo,
                FechaLanzamiento = this.FechaLanzamiento,
                Autor = contexto.Autor.Find(this.Autor.Id),
                Categoria = contexto.Categoria.Find(this.Categoria.Id),
                Editorial = contexto.Editorial.Find(this.Editorial.Id),
                Idioma = contexto.Idioma.Find(this.Idioma.Id),
                Paginas = this.Paginas,
                Descripcion = this.Descripcion,
                Peso = this.Peso
            };
        }
        public override DbSet ObtenerDbSet()
        {
            return contexto.Libro;
        }

        public override void LeerId(CapaDatos.Libro entity)
        {
            this.Id = entity.Id;
        }

        public override void EscribirDatos(CapaDatos.Libro entity)
        {
            entity.ISBN = this.ISBN;
            entity.Titulo = this.Titulo;
            entity.FechaLanzamiento = this.FechaLanzamiento;
            entity.Autor = contexto.Autor.Find(this.Autor.Id);
            entity.Categoria = contexto.Categoria.Find(this.Categoria.Id);
            entity.Editorial = contexto.Editorial.Find(this.Editorial.Id);
            entity.Idioma = contexto.Idioma.Find(this.Idioma.Id);
            entity.Paginas = this.Paginas;
            entity.Descripcion = this.Descripcion;
            entity.Peso = this.Peso;
        }

        public override IQueryable<CapaDatos.Libro> GenerarConsultaLinq(string filtro, BibliotecaEntities contexto)
        {
            return from l in contexto.Libro
                   where l.ISBN.Contains(filtro) ||
                        l.Titulo.Contains(filtro) ||
                        l.Autor.Apellidos.Contains(filtro) || l.Autor.Nombres.Contains(filtro) ||
                        l.Categoria.Nombre.Contains(filtro) ||
                        l.Editorial.Nombre.Contains(filtro) ||
                        l.Idioma.Nombre.Contains(filtro) ||
                        l.Descripcion.Contains(filtro)
                   select l;
        }

        public override void LeerDatos(CapaDatos.Libro entity)
        {
            LeerId(entity);
            this.ISBN = entity.ISBN;
            this.Titulo = entity.Titulo;
            this.FechaLanzamiento = (DateTime)entity.FechaLanzamiento;
            this.Autor.ObtenerPorId(entity.Autor.Id);
            this.Categoria.ObtenerPorId(entity.Categoria.Id);
            this.Editorial.ObtenerPorId(entity.Editorial.Id);
            this.Idioma.ObtenerPorId(entity.Idioma.Id);
            this.Paginas = entity.Paginas;
            this.Descripcion = entity.Descripcion;
            this.Peso = entity.Peso;
        }
        public override DataTable ObtenerDataTable(string filtro)
        {
            BibliotecaEntities contexto = Contexto.ObtenerInstancia();

            var consulta = GenerarConsultaLinq(filtro, contexto);

            return ListaADataTable(consulta.ToList());
        }
        private DataTable ListaADataTable(IList lista)
        {
            DataTable tabla = new DataTable();

            tabla.Columns.Add("Id");
            tabla.Columns.Add("Titulo");
            tabla.Columns.Add("Categoria");
            tabla.Columns.Add("Autor");
            tabla.Columns.Add("Editorial");
            tabla.Columns.Add("Descripcion");
            tabla.Columns.Add("Idioma");
            tabla.Columns.Add("Paginas");
            tabla.Columns.Add("Peso");

            foreach (Libro libro in lista)
            {
                DataRow row = tabla.NewRow();
                row["Id"] = libro.Id;
                row["Titulo"] = libro.Titulo;
                row["Categoria"] = libro.Categoria.Nombre;
                row["Autor"] = libro.Autor.Apellidos + ", " + libro.Autor.Nombres;
                row["Editorial"] = libro.Editorial.Nombre;
                row["Descripcion"] = libro.Descripcion;
                row["Idioma"] = libro.Idioma.Nombre;
                row["Paginas"] = libro.Paginas;
                row["Peso"] = libro.Peso;
                tabla.Rows.Add(row);
            }
            return tabla;
        }
    }
}