﻿using GestionBiblioteca.CapaNegocio;

namespace GestionBiblioteca
{
    public partial class SocioABM : FormularioABM
    {
        private SocioNeg entidad;
        protected override void BloquearEdicionCampos()
        {
            txtId.ReadOnly = true;
            txtDNI.ReadOnly = true;
            txtApellidos.ReadOnly = true;
            txtNombres.ReadOnly = true;
            txtTelefono.ReadOnly = true;
            txtDireccion.ReadOnly = true;
            txtCodigoPostal.ReadOnly = true;
            txtEmail.ReadOnly = true;
            txtOvservaciones.ReadOnly = true;
        }
        protected override void CargarCampos()
        {
            txtId.Text = entidad.Id.ToString().Trim();
            txtDNI.Text = entidad.DNI.ToString().Trim();
            txtApellidos.Text = entidad.Apellidos.Trim();
            txtNombres.Text = entidad.Nombres.Trim();
            txtTelefono.Text = entidad.Telefono.Trim();
            txtDireccion.Text = entidad.Direccion.Trim();
            txtCodigoPostal.Text = entidad.CodigoPostal.Trim();
            txtEmail.Text = entidad.Email.Trim();
            txtOvservaciones.Text = entidad.Observaciones.Trim();
        }
        protected override void ConfigurarAlta()
        {
            Title = "Alta de Socio";
        }
        protected override void ConfigurarBaja()
        {
            Title = "Eliminar Socio";
            BloquearEdicionCampos();
        }
        protected override void ConfigurarModificacion()
        {
            Title = "Editar Socio";
        }
        protected override void CrearObjeto()
        {
            BibliotecaFactory factory = BibliotecaFactory.ObtenerInstancia();
            entidad = factory.CrearSocio();
        }
        protected override void DarAlta()
        {
            entidad.Alta();
        }
        protected override void DarBaja()
        {
            entidad.Baja();
        }
        protected override void GuardarCambios()
        {
            entidad.Modificacion();
        }
        protected override void InicializarMensajes()
        {
            ucMensajes.Inicializar();
        }
        protected override void LeerCampos()
        {
            int.TryParse(txtId.Text, out int socioId);
            entidad.Id = socioId;
            entidad.DNI = int.Parse(txtDNI.Text);
            entidad.Apellidos = txtApellidos.Text;
            entidad.Nombres = txtNombres.Text;
            entidad.Telefono = txtTelefono.Text;
            entidad.Direccion = txtCodigoPostal.Text;
            entidad.CodigoPostal = txtCodigoPostal.Text;
            entidad.Email = txtEmail.Text;
            entidad.Observaciones = txtOvservaciones.Text;
        }
        protected override void LimpiarFormulario()
        {
            txtId.Text = "";
            txtDNI.Text = "";
            txtApellidos.Text = "";
            txtNombres.Text = "";
            txtTelefono.Text = "";
            txtDireccion.Text = "";
            txtCodigoPostal.Text = "";
            txtEmail.Text = "";
            txtOvservaciones.Text = "";
        }
        protected override void MensajeAltaError()
        {
            ucMensajes.MostrarError();
        }
        protected override void MensajeAltaOK()
        {
            ucMensajes.MostrarOk("Alta de Socio efectuada correctamente.");
        }
        protected override void MensajeBajaOK()
        {
            ucMensajes.MostrarOk("Baja de Socio efectuada correctamente.");
        }
        protected override void MensajeModificacionOK()
        {
            ucMensajes.MostrarOk("Edición de Socio efectuada correctamente.");
        }
        protected override void ObtenerEntidad()
        {
            entidad.ObtenerPorId(Id);
        }
        protected override void OcultarFormulario()
        {
            PanelFormulario.Visible = false;
            PanelContinuar.Visible = true;
        }
        protected override bool ReglasNegocioOK()
        {
            return Validador.ValidarReglasNegocio(entidad, ucMensajes);
        }
    }
}