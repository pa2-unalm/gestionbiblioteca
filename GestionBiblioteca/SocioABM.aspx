﻿<%@ Page Title="Alta de Socio" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SocioABM.aspx.cs" Inherits="GestionBiblioteca.SocioABM" %>
<%@ Register src="~/ControlesDeUsuario/UCMensajeUsuario.ascx" tagname="UCMensajeUsuario" tagprefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <div class="row d-flex">
        <div class="col-md-12 form-group justify-content-center">
            <uc:UCMensajeUsuario ID="ucMensajes" runat="server" Visible="False"></uc:UCMensajeUsuario>
        </div>
    </div>
    <asp:Panel ID="PanelFormulario" runat="server">
        <div class="row d-flex">
            <div class="col-md-4 form-group justify-content-center">
                <asp:TextBox ID="txtId" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
            
                <asp:Label ID="Label1" runat="server" Text="DNI" CssClass="bmd-label-floating" AssociatedControlID="txtDNI"></asp:Label>
                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDNI" ErrorMessage="Debe completar este campo." ForeColor="#CC0000"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtDNI" runat="server" CssClass="form-control"></asp:TextBox>
            
                <asp:Label ID="Label3" runat="server" Text="Apellido/s" AssociatedControlID="txtApellidos"></asp:Label>
                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtApellidos" ErrorMessage="Debe completar este campo." ForeColor="#CC0000"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtApellidos" runat="server" CssClass="form-control"></asp:TextBox>
            
                <asp:Label ID="Label2" runat="server" Text="Nombre/s" AssociatedControlID="txtNombres"></asp:Label>
                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtNombres" ErrorMessage="Debe completar este campo." ForeColor="#CC0000"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtNombres" runat="server" CssClass="form-control"></asp:TextBox>
            
                <asp:Label ID="Label4" runat="server" Text="Teléfono" AssociatedControlID="txtTelefono"></asp:Label>
                <asp:TextBox ID="txtTelefono" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-md-4 form-group justify-content-center">
                <asp:Label ID="Label13" runat="server" Text="Dirección" AssociatedControlID="txtDireccion"></asp:Label>
                <asp:TextBox ID="txtDireccion" runat="server" CssClass="form-control"></asp:TextBox>

                <asp:Label ID="Label14" runat="server" Text="Código Postal" AssociatedControlID="txtCodigoPostal"></asp:Label>
                <asp:TextBox ID="txtCodigoPostal" runat="server" CssClass="form-control"></asp:TextBox>
    
                <asp:Label ID="Label15" runat="server" Text="e-mail" AssociatedControlID="txtEmail"></asp:Label>
                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtEmail" ErrorMessage="Debe completar este campo." ForeColor="#CC0000"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
            
                <asp:Label ID="Label16" runat="server" Text="Observaciones" AssociatedControlID="txtOvservaciones"></asp:Label>
                <asp:TextBox ID="txtOvservaciones" runat="server" Rows="3" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
        <div class="row d-flex">
            <div class="col-md-4 justify-content-end">
                <asp:Button ID="btnOk" runat="server" Text="Ok" UseSubmitBehavior="true" CssClass="btn btn-primary"/>
                <input id="btnCancelar" type="button" value="Cancelar" onclick="window.history.back()" class="btn btn-secondary" />
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="PanelContinuar" runat="server" Visible="false" >
        <div class="row d-flex">
            <div class="col-md-4 justify-content-end">
                <asp:Button ID="btnContinuar" runat="server" Text="Continuar" UseSubmitBehavior="true" CssClass="btn btn-primary" PostBackUrl="~/SociosConsulta.aspx"/>
            </div>
        </div>
    </asp:Panel>
</asp:Content>