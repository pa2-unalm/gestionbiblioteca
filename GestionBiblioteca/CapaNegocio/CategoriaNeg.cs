﻿
using GestionBiblioteca.CapaDatos;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace GestionBiblioteca.CapaNegocio
{
    public class CategoriaNeg : EntidadNegocio<CapaDatos.Categoria>
    {
        public string Nombre { get; set; }
        
        public CategoriaNeg()
        {
            validador = new CategoriaValidador(this);
        }
        public override CapaDatos.Categoria CrearEntidadDatos()
        {
            return new CapaDatos.Categoria
            {
                Nombre = this.Nombre
            };
        }

        public override DbSet ObtenerDbSet()
        {
            return contexto.Categoria;
        }

        public override void LeerId(CapaDatos.Categoria entity)
        {
            this.Id = entity.Id;
        }

        public override void EscribirDatos(CapaDatos.Categoria entity)
        {
            entity.Nombre = this.Nombre;
        }

        public override IQueryable<CapaDatos.Categoria> GenerarConsultaLinq(string filtro, BibliotecaEntities contexto)
        {
            return from s in contexto.Categoria
                   where s.Nombre.Contains(filtro)
                   select s;
        }

        public override void LeerDatos(CapaDatos.Categoria entity)
        {
            LeerId(entity);
            this.Nombre = entity.Nombre;
        }
    }
}