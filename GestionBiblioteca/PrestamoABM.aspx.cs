﻿using GestionBiblioteca.CapaNegocio;
using System;

namespace GestionBiblioteca
{
    public partial class PrestamoABM : FormularioABM
    {
        private PrestamoNeg entidad;
        protected override void BloquearEdicionCampos()
        {
            txtId.ReadOnly = true;
            ddlSocio.Enabled = false;
            ddlLibro.Enabled = false;
            txtFechaSalida.ReadOnly = true;
            txtFechaEntrada.ReadOnly = true;
            txtObservaciones.ReadOnly = true;
        }
        protected override void CargarCampos()
        {
            txtId.Text = entidad.Id.ToString().Trim();
            ddlSocio.SelectedValue = entidad.Socio.Id.ToString();
            ddlLibro.SelectedValue = entidad.Libro.Id.ToString();
            txtFechaSalida.Text = entidad.FechaSalida.ToString().Trim();
            txtFechaEntrada.Text = entidad.FechaEntrada.ToString().Trim();
            txtObservaciones.Text = entidad.Observaciones.Trim();
        }
        protected override void ConfigurarAlta()
        {
            Title = "Alta de Préstamo";
        }
        protected override void ConfigurarBaja()
        {
            Title = "Eliminar Préstamo";
            BloquearEdicionCampos();
        }
        protected override void ConfigurarModificacion()
        {
            Title = "Editar Préstamo";
        }
        protected override void CrearObjeto()
        {
            BibliotecaFactory factory = BibliotecaFactory.ObtenerInstancia();
            entidad = factory.CrearPrestamo();
        }
        protected override void DarAlta()
        {
            entidad.Alta();
        }
        protected override void DarBaja()
        {
            entidad.Baja();
        }
        protected override void GuardarCambios()
        {
            entidad.Modificacion();
        }
        protected override void Inicializar()
        {
            base.Inicializar();
            InicializarDropDownLists();
        }
        private void InicializarDropDownLists()
        {
            Conversor.LlenarDropDownList(ddlSocio, entidad.Socio, "Apellidos");
            Conversor.LlenarDropDownList(ddlLibro, entidad.Libro, "Titulo");
        }
        protected override void InicializarMensajes()
        {
            ucMensajes.Inicializar();
        }
        protected override void LeerCampos()
        {
            int.TryParse(txtId.Text, out int prestamoId);
            entidad.Id = prestamoId;
            entidad.Socio.ObtenerPorId(int.Parse(ddlSocio.SelectedValue));
            entidad.Libro.ObtenerPorId(int.Parse(ddlLibro.SelectedValue));
            entidad.FechaSalida = DateTime.Parse(txtFechaSalida.Text);
            entidad.FechaEntrada = DateTime.Parse(txtFechaEntrada.Text);
            entidad.Observaciones = txtObservaciones.Text.Trim();
        }
        protected override void LimpiarFormulario()
        {
            txtId.Text = "";
            ddlSocio.SelectedIndex = 0;
            ddlLibro.SelectedIndex = 0;
            txtFechaSalida.Text = "";
            txtFechaEntrada.Text = "";
            txtObservaciones.Text = "";
        }
        protected override void MensajeAltaError()
        {
            ucMensajes.MostrarError();
        }
        protected override void MensajeAltaOK()
        {
            ucMensajes.MostrarOk("Alta de Préstamo efectuada correctamente.");
        }
        protected override void MensajeBajaOK()
        {
            ucMensajes.MostrarOk("Baja de Préstamo efectuada correctamente.");
        }
        protected override void MensajeModificacionOK()
        {
            ucMensajes.MostrarOk("Edición de Préstamo efectuada correctamente.");
        }
        protected override void ObtenerEntidad()
        {
            entidad.ObtenerPorId(Id);
        }
        protected override void OcultarFormulario()
        {
            PanelFormulario.Visible = false;
            PanelContinuar.Visible = true;
        }
        protected override bool ReglasNegocioOK()
        {
            return Validador.ValidarReglasNegocio(entidad, ucMensajes);
        }
    }
}