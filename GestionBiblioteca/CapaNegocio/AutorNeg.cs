﻿using GestionBiblioteca.CapaDatos;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace GestionBiblioteca.CapaNegocio
{
    public class AutorNeg : EntidadNegocio<CapaDatos.Autor>
    {
        public string Apellidos { get; set; }
        public string Nombres { get; set; }
        public string Email { get; set; }
        public ICollection<Libro> Libros { get; set; }

        public AutorNeg()
        {
            validador = new AutorValidador(this);
        }
        public override CapaDatos.Autor CrearEntidadDatos()
        {
            return new CapaDatos.Autor
            {
                Apellidos = this.Apellidos,
                Nombres = this.Nombres,
                Email = this.Email
            };
        }
        public override DbSet ObtenerDbSet()
        {
            return contexto.Autor;
        }
        public override void LeerId(CapaDatos.Autor entity)
        {
            this.Id = entity.Id;
        }
        public override void EscribirDatos(CapaDatos.Autor entity)
        {
            entity.Apellidos = this.Apellidos;
            entity.Nombres = this.Nombres;
            entity.Email = this.Email;
        }
        public override IQueryable<CapaDatos.Autor> GenerarConsultaLinq(string filtro, BibliotecaEntities contexto)
        {
            return from a in contexto.Autor
                   where a.Apellidos.Contains(filtro) ||
                        a.Nombres.Contains(filtro) ||
                        a.Email.Contains(filtro)
                   select a;
        }
        public override void LeerDatos(CapaDatos.Autor entity)
        {
            LeerId(entity);
            this.Apellidos = entity.Apellidos;
            this.Nombres = entity.Nombres;
            this.Email = entity.Email;
        }
    }
}