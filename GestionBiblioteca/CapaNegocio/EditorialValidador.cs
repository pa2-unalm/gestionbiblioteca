﻿using System.Collections.Generic;
using System.Linq;

namespace GestionBiblioteca.CapaNegocio
{
    public class EditorialValidador : Validador
    {
        private readonly EditorialNeg editorial;

        public EditorialValidador(IValidable entidad)
            : base(entidad)
        {
            this.editorial = (EditorialNeg)entidad;
        }
        public override Dictionary<string, ResultadoValidacion> Validar()
        {
            base.Validar();

            ValidarNombre("Nombre");

            return Resultados;
        }
        private void ValidarNombre(string campo)
        {
            NuevoResultadoValidacion(campo);

            if (Contexto.ObtenerInstancia().Editorial.Any(c => c.Nombre.Equals(editorial.Nombre)))
                PonerMensajeError("La Editorial '{0}' ya existe. ", editorial.Nombre.ToString(), campo);
        }
    }
}