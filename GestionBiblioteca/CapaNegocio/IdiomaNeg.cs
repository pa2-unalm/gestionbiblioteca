﻿
using GestionBiblioteca.CapaDatos;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace GestionBiblioteca.CapaNegocio
{
    public class IdiomaNeg : EntidadNegocio<CapaDatos.Idioma>
    {
        public string Nombre { get; set; }

        public IdiomaNeg()
        {
            validador = new IdiomaValidador(this);
        }
        public override CapaDatos.Idioma CrearEntidadDatos()
        {
            return new CapaDatos.Idioma
            {
                Nombre = this.Nombre
            };
        }

        public override DbSet ObtenerDbSet()
        {
            return contexto.Idioma;
        }

        public override void LeerId(CapaDatos.Idioma entity)
        {
            this.Id = entity.Id;
        }

        public override void EscribirDatos(CapaDatos.Idioma entity)
        {
            entity.Nombre = this.Nombre;
        }

        public override IQueryable<CapaDatos.Idioma> GenerarConsultaLinq(string filtro, BibliotecaEntities contexto)
        {
            return from s in contexto.Idioma
                   where s.Nombre.Contains(filtro)
                   select s;
        }

        public override void LeerDatos(CapaDatos.Idioma entity)
        {
            LeerId(entity);
            this.Nombre = entity.Nombre;
        }
    }
}