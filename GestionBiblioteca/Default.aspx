﻿<%@ Page Title="Inicio" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="GestionBiblioteca._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Gestión de Bibilioteca</h1>
        <p class="lead">Trabajo práctico final.</p>
        <p class="lead"><b>Docentes:</b> Juiz, Mariano // Garrido Bonnin, Flavio</p>
        <p class="lead"><b>Equipo de trabajo:</b> Coronel, Leila // Machuca, Martín // Martínez, Mariana // Martínez, Matías // Senatori, Emmanuel</p>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h2>Descripción</h2>
            <p>
                El presente proyecto es la implementación de un pequeño sistema de préstamos de libros. Se gestionan el mínimo de entidades que participan de un préstamo:
                Libro, Socio, Préstamos. También se contemplan entidades como Categoría, Editorial y Autor. Dichas entidades son persistidas en una base de datos relacional MS SQL Server, mediante el uso del ORM Entity Framework.
            </p>
        </div>
    </div>

</asp:Content>
