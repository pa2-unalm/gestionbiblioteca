﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionBiblioteca.CapaNegocio
{
    public class ResultadoValidacion 
    {
        public string Key { get; set; }
        public bool Resultado { get; set; }
        public string Mensaje { get; set; }

        public ResultadoValidacion() { }
        public ResultadoValidacion(string key, bool resultado, string mensaje)
            : this()
        {
            this.Key = key;
            this.Resultado = resultado;
            this.Mensaje = mensaje;
        }
    }
}