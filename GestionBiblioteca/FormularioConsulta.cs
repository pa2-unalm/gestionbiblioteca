﻿using GestionBiblioteca.CapaNegocio;
using GestionBiblioteca.ControlesDeUsuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionBiblioteca
{
    public abstract class FormularioConsulta : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Inicializar();
            Filtrar();
        }
        private void Inicializar()
        {
            ObtenerFormularioConsulta().Inicializar();
        }
        private void Filtrar()
        {
            IPersistible objetoNegocio = CrearObjetoNegocio();
            ObtenerFormularioConsulta().Filtrar(objetoNegocio);
        }
        protected abstract UCFormularioConsulta ObtenerFormularioConsulta();
        protected abstract IPersistible CrearObjetoNegocio();
    }
}