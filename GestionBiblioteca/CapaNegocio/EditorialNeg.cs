﻿
using GestionBiblioteca.CapaDatos;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace GestionBiblioteca.CapaNegocio
{
    public class EditorialNeg : EntidadNegocio<CapaDatos.Editorial>
    {
        public string Nombre { get; set; }

        public EditorialNeg()
        {
            validador = new EditorialValidador(this);
        }
        public override CapaDatos.Editorial CrearEntidadDatos()
        {
            return new CapaDatos.Editorial
                        {
                            Nombre = this.Nombre
                        };
        }

        public override DbSet ObtenerDbSet()
        {
            return contexto.Editorial;
        }

        public override void LeerId(CapaDatos.Editorial entity)
        {
            this.Id = entity.Id;
        }

        public override void EscribirDatos(CapaDatos.Editorial entity)
        {
            entity.Nombre = this.Nombre;
        }

        public override IQueryable<CapaDatos.Editorial> GenerarConsultaLinq(string filtro, BibliotecaEntities contexto)
        {
            return from e in contexto.Editorial
                   where e.Nombre.Contains(filtro)
                   select e;
        }

        public override void LeerDatos(CapaDatos.Editorial entity)
        {
            LeerId(entity);
            this.Nombre = entity.Nombre;
        }
    }
}