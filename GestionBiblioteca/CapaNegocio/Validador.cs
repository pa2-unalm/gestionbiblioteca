﻿using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionBiblioteca.CapaNegocio
{
    public abstract class Validador : IValidador
    {
        protected readonly Dictionary<string, ResultadoValidacion> Resultados;
        protected readonly IValidable entidad;

        public Validador(IValidable entidad)
        {
            this.Resultados = new Dictionary<string, ResultadoValidacion>();
            this.entidad = (IValidable)entidad;
        }

        public virtual Dictionary<string, ResultadoValidacion> Validar()
        {
            Resultados.Clear();

            return Resultados;
        }

        protected void NuevoResultadoValidacion(string campo)
        {
            Resultados.Add(campo, new ResultadoValidacion(campo, true, ""));
        }
        protected void PonerMensajeError(string mensaje, string valor, string campo)
        {
            Resultados[campo].Mensaje = string.Format(mensaje, valor);
            Resultados[campo].Resultado = false;
        }
    }
}