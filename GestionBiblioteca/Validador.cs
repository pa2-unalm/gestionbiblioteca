﻿using GestionBiblioteca.CapaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace GestionBiblioteca
{
    public class Validador
    {
        public static bool ValidarReglasNegocio(IValidable entidad, MensajeUsuario ucMensajes)
        {
            bool resultado = true;

            Dictionary<string, ResultadoValidacion> validacion = entidad.Validar();

            foreach (KeyValuePair<string, ResultadoValidacion> elemento in validacion)
            {
                if (!elemento.Value.Resultado)
                {
                    ucMensajes.AgregarMensaje(elemento.Value.Mensaje);
                    resultado = false;
                }
            }
            return resultado;
        }
    }
}