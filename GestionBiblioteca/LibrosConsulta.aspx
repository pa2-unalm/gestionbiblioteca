﻿<%@ Page Title="Libros" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="LibrosConsulta.aspx.cs" Inherits="GestionBiblioteca.LibrosConsulta" %>
<%@ Register src="~/ControlesDeUsuario/UCFormularioConsulta.ascx" tagname="UCFormularioConsulta" tagprefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <uc:UCFormularioConsulta ID="ucFormularioConsulta" runat="server" Titulo="Libros" NombreFormularioABM="LibroABM.aspx"></uc:UCFormularioConsulta>
</asp:Content>