﻿using Microsoft.Ajax.Utilities;
using System.Collections.Generic;
using System.Linq;

namespace GestionBiblioteca.CapaNegocio
{
    public class SocioValidador : Validador
    {
        private readonly SocioNeg socio;

        public SocioValidador(IValidable entidad)
            : base(entidad)
        {

            this.socio = (SocioNeg)entidad;
        }
        public override Dictionary<string, ResultadoValidacion> Validar()
        {
            base.Validar();

            ValidarDNI("DNI");
            ValidarEmail("EMail");

            return Resultados;
        }
        private void ValidarDNI(string campo)
        {
            NuevoResultadoValidacion(campo);

            if (socio.DNI < 1_000_000)
                PonerMensajeError("El campo {0} tiene un formato inválido. ", campo, campo);

            if (Contexto.ObtenerInstancia().Socio.Any(s => s.DNI == socio.DNI))
                PonerMensajeError("El DNI '{0}' ya existe. ", socio.DNI.ToString(), campo);
        }
        private void ValidarEmail(string campo)
        {
            NuevoResultadoValidacion(campo);

            if (socio.Email.IsNullOrWhiteSpace() || !socio.Email.Contains("@"))
                PonerMensajeError("El campo {0} tiene un formato inválido. ", campo, campo);

            if (Contexto.ObtenerInstancia().Socio.Any(s => s.Email.Equals(socio.Email)))
                PonerMensajeError("El email '{0}' ya existe. ", socio.Email, campo);
        }
    }
}