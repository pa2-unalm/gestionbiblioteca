﻿using System;

namespace GestionBiblioteca
{
    public abstract class FormularioABM : System.Web.UI.Page
    {
        protected const string ALTA = "ALTA";
        protected const string MODIFICACION = "MODIFICACION";
        protected const string BAJA = "BAJA";

        protected int Id = 0;
        protected string accion = ALTA;

        protected void Page_Load(object sender, EventArgs e)
        {
            CrearObjeto();
            Inicializar();

            if (!Page.IsPostBack)
            {
                if (accion.Equals(ALTA))
                    ConfigurarAlta();
                else
                {
                    ObtenerEntidad();
                    CargarCampos();

                    if (accion.Equals(MODIFICACION))
                        ConfigurarModificacion();
                    else if (accion.Equals(BAJA))
                        ConfigurarBaja();
                }
            }
            else
            {
                LeerCampos();

                if (accion.Equals(ALTA))
                {
                    if (ReglasNegocioOK())
                    {
                        ConfigurarAlta();
                        DarAlta();
                        MensajeAltaOK();
                        LimpiarFormulario();
                    }
                    else
                        MensajeAltaError();
                }
                else if (accion.Equals(MODIFICACION))
                {
                    ConfigurarModificacion();
                    GuardarCambios();
                    MensajeModificacionOK();
                }
                else if (accion.Equals(BAJA))
                {
                    ConfigurarBaja();
                    DarBaja();
                    MensajeBajaOK();
                    OcultarFormulario();
                }
            }
        }
        protected virtual void Inicializar()
        {
            InicializarMensajes();

            if (Request.Params["accion"] != null)
                accion = Request.Params["accion"].ToUpper().Trim();

            if (Request.Params["id"] != null)
                int.TryParse(Request.Params["id"], out Id);
        }
        protected abstract void InicializarMensajes();
        protected abstract void CrearObjeto();
        protected abstract void ObtenerEntidad();
        protected abstract void CargarCampos();
        protected abstract void LeerCampos();
        protected abstract bool ReglasNegocioOK();
        protected abstract void DarAlta();
        protected abstract void MensajeAltaOK();
        protected abstract void GuardarCambios();
        protected abstract void MensajeModificacionOK();
        protected abstract void MensajeAltaError();
        protected abstract void DarBaja();
        protected abstract void MensajeBajaOK();
        protected abstract void LimpiarFormulario();
        protected abstract void OcultarFormulario();
        protected abstract void ConfigurarAlta();
        protected abstract void ConfigurarModificacion();
        protected abstract void ConfigurarBaja();
        protected abstract void BloquearEdicionCampos();
    }
}