﻿using GestionBiblioteca.CapaNegocio;
using System;
using System.Web.UI;

namespace GestionBiblioteca
{
    public partial class CategoriaABM : FormularioABM
    {
        private CategoriaNeg entidad;
        protected override void BloquearEdicionCampos()
        {
            txtId.ReadOnly = true;
            txtNombre.ReadOnly = true;
        }

        protected override void CargarCampos()
        {
            txtId.Text = entidad.Id.ToString().Trim();
            txtNombre.Text = entidad.Nombre.Trim();
        }

        protected override void ConfigurarAlta()
        {
            Title = "Alta de Categoría";
        }

        protected override void ConfigurarBaja()
        {
            Title = "Eliminar Categoría";
            BloquearEdicionCampos();
        }

        protected override void ConfigurarModificacion()
        {
            Title = "Editar Categoría";
        }

        protected override void CrearObjeto()
        {
            BibliotecaFactory factory = BibliotecaFactory.ObtenerInstancia();
            entidad = (CategoriaNeg)factory.CrearCategoria();
        }

        protected override void DarAlta()
        {
            entidad.Alta();
        }

        protected override void DarBaja()
        {
            entidad.Baja();
        }

        protected override void GuardarCambios()
        {
            entidad.Modificacion();
        }

        protected override void InicializarMensajes()
        {
            ucMensajes.Inicializar();
        }

        protected override void LeerCampos()
        {
            int.TryParse(txtId.Text, out int entidadId);
            entidad.Id = entidadId;
            entidad.Nombre = txtNombre.Text;
        }

        protected override void LimpiarFormulario()
        {
            txtId.Text = "";
            txtNombre.Text = "";
        }

        protected override void MensajeAltaError()
        {
            ucMensajes.MostrarError();
        }

        protected override void MensajeAltaOK()
        {
            ucMensajes.MostrarOk("Alta de Categoría efectuada correctamente.");
        }

        protected override void MensajeBajaOK()
        {
            ucMensajes.MostrarOk("Baja de Categoría efectuada correctamente.");
        }

        protected override void MensajeModificacionOK()
        {
            ucMensajes.MostrarOk("Edición de Categoría efectuada correctamente.");
        }

        protected override void ObtenerEntidad()
        {
            entidad.ObtenerPorId(Id);
        }

        protected override void OcultarFormulario()
        {
            PanelFormulario.Visible = false;
            PanelContinuar.Visible = true;
        }

        protected override bool ReglasNegocioOK()
        {
            return Validador.ValidarReglasNegocio(entidad, ucMensajes);
        }
    }
}