﻿using GestionBiblioteca.CapaDatos;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GestionBiblioteca.CapaNegocio
{
    public class Contexto
    {
        #region Patrón Singleton
        private static BibliotecaEntities instancia;
        private Contexto() { }
        public static BibliotecaEntities ObtenerInstancia()
        {
            if (instancia == null)
                instancia = new BibliotecaEntities();

            return instancia;
        }
        #endregion
    }
}