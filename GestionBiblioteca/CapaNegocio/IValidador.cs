﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionBiblioteca.CapaNegocio
{
    public interface IValidador
    {
        public Dictionary<string, ResultadoValidacion> Validar();
    }
}
