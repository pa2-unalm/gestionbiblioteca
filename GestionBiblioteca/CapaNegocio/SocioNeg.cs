﻿
using GestionBiblioteca.CapaDatos;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace GestionBiblioteca.CapaNegocio
{
    public class SocioNeg : EntidadNegocio<CapaDatos.Socio>
    {
        public int? DNI { get; set; }
        public string Apellidos { get; set; }
        public string Nombres { get; set; }
        public string Telefono { get; set; }
        public string Direccion { get; set; }
        public string CodigoPostal { get; set; }
        public string Observaciones { get; set; }
        public string Email { get; set; }

        public SocioNeg()
        {
            validador = new SocioValidador(this);
        }
        public override CapaDatos.Socio CrearEntidadDatos()
        {
            return new CapaDatos.Socio
            {
                DNI = this.DNI,
                Apellidos = this.Apellidos,
                Nombres = this.Nombres,
                Telefono = this.Telefono,
                Direccion = this.Direccion,
                CodigoPostal = this.CodigoPostal,
                Email = this.Email,
                Observaciones = this.Observaciones
            };
        }
        public override DbSet ObtenerDbSet()
        {
            return contexto.Socio;
        }
        public override void LeerId(CapaDatos.Socio entity)
        {
            this.Id = entity.Id;
        }
        public override void EscribirDatos(CapaDatos.Socio entity)
        {
            entity.DNI = this.DNI;
            entity.Apellidos = this.Apellidos;
            entity.Nombres = this.Nombres;
            entity.Telefono = this.Telefono;
            entity.Direccion = this.Direccion;
            entity.CodigoPostal = this.CodigoPostal;
            entity.Email = this.Email;
            entity.Observaciones = this.Observaciones;
        }
        public override IQueryable<CapaDatos.Socio> GenerarConsultaLinq(string filtro, BibliotecaEntities contexto)
        {
            return from s in contexto.Socio
                   where s.DNI.ToString().Contains(filtro) ||
                        s.Apellidos.Contains(filtro) ||
                        s.Nombres.Contains(filtro) ||
                        s.Direccion.Contains(filtro) ||
                        s.Email.Contains(filtro) ||
                        s.Observaciones.Contains(filtro)
                   select s;
        }
        public override void LeerDatos(CapaDatos.Socio entity)
        {
            LeerId(entity);
            this.DNI = entity.DNI;
            this.Apellidos = entity.Apellidos;
            this.Nombres = entity.Nombres;
            this.Telefono = entity.Telefono;
            this.Direccion = entity.Direccion;
            this.CodigoPostal = entity.CodigoPostal;
            this.Email = entity.Email;
            this.Observaciones = entity.Observaciones;
        }
    }
}