﻿using System.Collections.Generic;
using System.Linq;

namespace GestionBiblioteca.CapaNegocio
{
    public class LibroValidador : Validador
    {
        private readonly LibroNeg libro;

        public LibroValidador(IValidable entidad)
            : base(entidad)
        {

            this.libro = (LibroNeg)entidad;
        }
        public override Dictionary<string, ResultadoValidacion> Validar()
        {
            base.Validar();

            ValidarISBN("ISBN");

            return Resultados;
        }
        private void ValidarISBN(string campo)
        {
            NuevoResultadoValidacion(campo);

            if (libro.ISBN.Length < 10)
                PonerMensajeError("El campo {0} tiene un formato inválido. ", campo, campo);

            if (Contexto.ObtenerInstancia().Libro.Any(l => l.ISBN == libro.ISBN))
                PonerMensajeError("El ISBN '{0}' ya existe. ", libro.ISBN.ToString(), campo);
        }
    }
}