﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionBiblioteca.CapaNegocio
{
    public class BibliotecaFactory 
    {
        #region Patrón Singleton
        private static BibliotecaFactory instancia;
        
        private BibliotecaFactory()
        {
        }
        public static BibliotecaFactory ObtenerInstancia()
        {
            if (instancia == null)
                instancia = new BibliotecaFactory();

            return instancia;
        }
        #endregion

        public AutorNeg CrearAutor()
        {
            return new AutorNeg();
        }

        public CategoriaNeg CrearCategoria()
        {
            return new CategoriaNeg();
        }

        public EditorialNeg CrearEditorial()
        {
            return new EditorialNeg();
        }

        public LibroNeg CrearLibro()
        {
            return new LibroNeg();
        }

        public PrestamoNeg CrearPrestamo()
        {
            return new PrestamoNeg();
        }

        public SocioNeg CrearSocio()
        {
            return new SocioNeg();
        }
    }
}