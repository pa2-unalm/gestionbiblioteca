﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCMensajeUsuario.ascx.cs" Inherits="GestionBiblioteca.MensajeUsuario" %>
<asp:Panel ID="PanelPrincipal" runat="server" CssClass="alert alert-success" role="alert">
    <asp:Panel ID="PanelIcono" runat="server" CssClass="glyphicon glyphicon-ok" Visible="false"></asp:Panel>
    <asp:Label ID="LabelMensaje" runat="server" Text="" ></asp:Label>
</asp:Panel>
