﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCFormularioConsulta.ascx.cs" Inherits="GestionBiblioteca.ControlesDeUsuario.UCFormularioConsulta" %>
<%@ Register src="~/ControlesDeUsuario/UCMensajeUsuario.ascx" tagname="UCMensajeUsuario" tagprefix="uc" %>

    <h2><%: Titulo %></h2>
    
    <div class="row d-flex">
        <div class="col-md-3 form-group">
            <asp:TextBox ID="txtBuscar" runat="server" CssClass="form-static " AutoPostBack="True"></asp:TextBox>
            <asp:Button ID="butBuscar" runat="server" CssClass="btn btn-primary " Text="Buscar" />
        </div>
        <div class="col-md-7">
            <uc:UCMensajeUsuario ID="ucMensajes" runat="server" Visible="False"></uc:UCMensajeUsuario>
        </div>
    </div>
    <div class="row d-flex">
        <div class="col-md-12 form-group justify-content-center">
            <asp:GridView ID="GridView1" runat="server" CssClass="table table-responsive" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Horizontal">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate >
                            <asp:LinkButton 
                                runat="server" ID="link" ToolTip="Editar registro" 
                                PostBackUrl='<%# string.Format("~/{0}?id={1}&accion=MODIFICACION", NombreFormularioABM, Eval("Id")) %>'><span class="glyphicon glyphicon-pencil"></span></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate >
                            <asp:LinkButton 
                                runat="server" ID="LinkButton1" ToolTip="Eliminar registro"
                                PostBackUrl='<%# string.Format("~/{0}?id={1}&accion=BAJA", NombreFormularioABM, Eval("Id")) %>'><span class="glyphicon glyphicon-remove" style="color:darkred"></span></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                <SortedDescendingHeaderStyle BackColor="#242121" />
            </asp:GridView>
        </div>
    </div>
    <div class="row d-flex">
        <div class="col-md-12 form-group">
            <asp:Button ID="Agregar" runat="server" CssClass="btn btn-default" Text="Agregar..." ></asp:Button>
        </div>
    </div>
