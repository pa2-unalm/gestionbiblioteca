﻿<%@ Page Title="Alta de Categoría" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CategoriaABM.aspx.cs" Inherits="GestionBiblioteca.CategoriaABM" %>
<%@ Register src="~/ControlesDeUsuario/UCMensajeUsuario.ascx" tagname="UCMensajeUsuario" tagprefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <div class="row d-flex">
        <div class="col-md-12 form-group justify-content-center">
            <uc:UCMensajeUsuario ID="ucMensajes" runat="server" Visible="False"></uc:UCMensajeUsuario>
        </div>
    </div>
    <asp:Panel ID="PanelFormulario" runat="server">
        <div class="row d-flex">
            <div class="col-md-4 form-group justify-content-center">
                <asp:TextBox ID="txtId" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
            
                <asp:Label ID="Label3" runat="server" Text="Nombre de la Categoría" AssociatedControlID="txtNombre"></asp:Label>
                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtNombre" ErrorMessage="Debe completar este campo." ForeColor="#CC0000"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="row d-flex">
            <div class="col-md-4 justify-content-end">
                <asp:Button ID="btnOk" runat="server" Text="Ok" UseSubmitBehavior="true" CssClass="btn btn-primary"/>
                <input id="btnCancelar" type="button" value="Cancelar" onclick="window.history.back()" class="btn btn-secondary" />
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="PanelContinuar" runat="server" Visible="false" >
        <div class="row d-flex">
            <div class="col-md-4 justify-content-end">
                <asp:Button ID="btnContinuar" runat="server" Text="Continuar" UseSubmitBehavior="true" CssClass="btn btn-primary" PostBackUrl="~/CategoriasConsulta.aspx"/>
            </div>
        </div>
    </asp:Panel>
</asp:Content>