﻿using GestionBiblioteca.CapaNegocio;
using System;
using System.Data;

namespace GestionBiblioteca.ControlesDeUsuario
{
    public partial class UCFormularioConsulta : System.Web.UI.UserControl
    {
        public string Titulo { get; set; }
        public string NombreFormularioABM { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            Agregar.PostBackUrl = "~/" + NombreFormularioABM;
        }
        public void Inicializar()
        {
            ucMensajes.Inicializar();
        }
        public void Filtrar(IPersistible manejador)
        {
            DataTable datos = manejador.ObtenerDataTable(txtBuscar.Text);
            GridView1.DataSource = datos;
            GridView1.DataBind();

            if (datos.Rows.Count == 0)
                ucMensajes.MostrarInfo("No se encontraron registros.");
        }
    }
}