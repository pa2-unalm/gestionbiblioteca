﻿using GestionBiblioteca.ControlesDeUsuario;
using GestionBiblioteca.CapaNegocio;

namespace GestionBiblioteca
{
    public partial class PrestamosConsulta : FormularioConsulta
    {
        protected override IPersistible CrearObjetoNegocio()
        {
            return BibliotecaFactory.ObtenerInstancia().CrearPrestamo();
        }
        protected override UCFormularioConsulta ObtenerFormularioConsulta()
        {
            return ucFormularioConsulta;
        }
    }
}