﻿using System.Collections.Generic;
using System.Linq;

namespace GestionBiblioteca.CapaNegocio
{
    public class IdiomaValidador : Validador
    {
        private readonly IdiomaNeg idioma;

        public IdiomaValidador(IValidable entidad)
            : base(entidad)
        {
            this.idioma = (IdiomaNeg)entidad;
        }
        public override Dictionary<string, ResultadoValidacion> Validar()
        {
            base.Validar();

            ValidarNombre("Nombre");

            return Resultados;
        }
        private void ValidarNombre(string campo)
        {
            NuevoResultadoValidacion(campo);

            if (Contexto.ObtenerInstancia().Idioma.Any(c => c.Nombre.Equals(idioma.Nombre)))
                PonerMensajeError("El Idioma '{0}' ya existe. ", idioma.Nombre.ToString(), campo);
        }
    }
}