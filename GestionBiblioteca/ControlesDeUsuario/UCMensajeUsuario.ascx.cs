﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GestionBiblioteca
{
    public partial class MensajeUsuario : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void Inicializar()
        {
            LabelMensaje.Text = "";
            Visible = false;
        }
        public void AgregarMensaje(string texto)
        {
            if (texto != null)
                LabelMensaje.Text += "{0}  " + texto + "<br/>";
        }
        public void MostrarError()
        {
            MostrarError(null);
        }
        public void MostrarError(string texto)
        {
            AgregarMensaje(texto);
            PanelPrincipal.CssClass = "alert alert-danger";
            LabelMensaje.Text = string.Format(LabelMensaje.Text, "<span class='glyphicon glyphicon-minus-sign'></span>");
            this.Visible = true;
        }
        public void MostrarOk()
        {
            MostrarOk(null);
        }
        public void MostrarOk(string texto)
        {
            AgregarMensaje(texto);
            PanelPrincipal.CssClass = "alert alert-success";
            LabelMensaje.Text = string.Format(LabelMensaje.Text, "<span class='glyphicon glyphicon-ok-sign'></span>");
            this.Visible = true;
        }
        public void MostrarInfo()
        {
            MostrarInfo(null);
        }
        public void MostrarInfo(string texto)
        {
            AgregarMensaje(texto);
            PanelPrincipal.CssClass = "alert alert-info";
            LabelMensaje.Text = string.Format(LabelMensaje.Text, "<span class='glyphicon glyphicon-info-sign'></span>");
            this.Visible = true;
        }
    }
}