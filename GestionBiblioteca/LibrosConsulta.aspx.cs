﻿using GestionBiblioteca.ControlesDeUsuario;
using GestionBiblioteca.CapaNegocio;

namespace GestionBiblioteca
{
    public partial class LibrosConsulta : FormularioConsulta
    {
        protected override IPersistible CrearObjetoNegocio()
        {
            return BibliotecaFactory.ObtenerInstancia().CrearLibro();
        }
        protected override UCFormularioConsulta ObtenerFormularioConsulta()
        {
            return ucFormularioConsulta;
        }
    }
}