﻿<%@ Page Title="Socios" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SociosConsulta.aspx.cs" Inherits="GestionBiblioteca.SociosConsulta" %>
<%@ Register src="~/ControlesDeUsuario/UCFormularioConsulta.ascx" tagname="UCFormularioConsulta" tagprefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <uc:UCFormularioConsulta ID="ucFormularioConsulta" runat="server" Titulo="Socios" NombreFormularioABM="SocioABM.aspx"></uc:UCFormularioConsulta>
</asp:Content>