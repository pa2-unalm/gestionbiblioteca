﻿<%@ Page Title="Prestamos" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PrestamosConsulta.aspx.cs" Inherits="GestionBiblioteca.PrestamosConsulta" %>
<%@ Register src="~/ControlesDeUsuario/UCFormularioConsulta.ascx" tagname="UCFormularioConsulta" tagprefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <uc:UCFormularioConsulta ID="ucFormularioConsulta" runat="server" Titulo="Prestamos" NombreFormularioABM="PrestamoABM.aspx"></uc:UCFormularioConsulta>
</asp:Content>